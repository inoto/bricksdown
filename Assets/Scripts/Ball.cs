﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

public class Ball : MonoBehaviour
{
    public event Action BallLostEvent;

    [SerializeField] float Speed = 2f;
    
    Vector3 velocity;
    
    Camera _camera;
    Transform _transform;
    Rigidbody _rigidbody;
    TrailRenderer _trail;

    Vector3 pos;

    void Awake()
    {
        _transform = GetComponent<Transform>();
        _rigidbody = GetComponent<Rigidbody>();
        _trail = GetComponent<TrailRenderer>();
        _camera = Camera.main;
    }

    public Vector3 Init(Vector3 position)
    {
        _rigidbody.velocity = Vector3.zero;
        _rigidbody.angularVelocity = Vector3.zero;
        _trail.Clear();

        pos = _camera.ScreenToWorldPoint(
            new Vector3(position.x, position.y, 10));
        velocity = Vector3.forward * Speed + pos * Speed/10;
        _rigidbody.AddForce(velocity, ForceMode.Impulse);
        return velocity;
    }

    void Update()
    {
        if (_transform.position.y < -5)
        {
            BallLostEvent?.Invoke();
            SimplePool.Despawn(gameObject);
        }
    }
}
