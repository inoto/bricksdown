﻿using System;
using UnityEngine;

public class MouseController : MonoBehaviour
{
    public static event Action<Vector3> OnMouseClickedEvent;

    void Start()
    {
#if !UNITY_EDITOR
		Destroy(this);
#endif
    }

    void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            OnMouseClickedEvent?.Invoke(Input.mousePosition);
        }
    }
}
