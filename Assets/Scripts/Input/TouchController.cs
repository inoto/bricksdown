﻿using System;
using UnityEngine;

public class TouchController : MonoBehaviour
{
    public static event Action<Vector3> OnTouchEvent;

    Touch touch;

    void Start()
    {
#if UNITY_EDITOR
        Destroy(this);
#endif
    }

    void Update()
    {
        if (Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Ended)
            {
                OnTouchEvent?.Invoke(touch.position);
            }
        }
    }
}