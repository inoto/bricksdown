﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour
{
    public event Action<Cube> DestroyedEvent;

    Transform _transform;
    
    void Awake()
    {
        _transform = GetComponent<Transform>();
    }

    void Update()
    {
        if (_transform.position.y < 0)
        {
            Destroy(gameObject, 1f);
            DestroyedEvent?.Invoke(this);
        }
    }
}
