﻿using TMPro;
using UnityEngine;

public class UIBalls : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI BallsCountText;

    void Start()
    {
        UpdateBallsCount(0);
        UILevel.LevelChangedEvent += OnLevelChangedEvent;
        UILevel.OnProgressStepEvent += OnProgressStepEvent;
        Cannon.BallsCountChangedEvent += OnBallsCountChangedEvent;
    }

    void OnDestroy()
    {
        UILevel.LevelChangedEvent -= OnLevelChangedEvent;
        UILevel.OnProgressStepEvent -= OnProgressStepEvent;
        Cannon.BallsCountChangedEvent -= OnBallsCountChangedEvent;
    }

    void OnBallsCountChangedEvent(int ballsCount)
    {
        UpdateBallsCount(ballsCount);
    }

    void OnLevelChangedEvent(int level, int nextLevel)
    {
        UpdateBallsCount(Cannon.StartBallsCount * nextLevel);
    }

    void OnProgressStepEvent(int level)
    {
        UpdateBallsCount(Cannon.StartBallsCount * level);
    }

    void UpdateBallsCount(int ballsCount)
    {
        BallsCountText.text = $"x {ballsCount}";
    }
}
