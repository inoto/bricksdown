﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour
{
    public static event Action<int> BallsCountChangedEvent;
    public static event Action<int> BallLostEvent;
    public const int StartBallsCount = 3;

    [SerializeField] GameObject BallPrefab;

    Transform _transform;

    Vector3 startPosition;
    int ballsCount = 0;

    void Awake()
    {
        _transform = GetComponent<Transform>();
    }
    void Start()
    {
        SimplePool.Preload(BallPrefab, 20);
        startPosition = _transform.position;

        UILevel.LevelChangedEvent += OnLevelChangedEvent;
        UILevel.OnProgressStepEvent += OnProgressStepEvent;
        UILevel.EndOfGameEvent += OnEndOfGameEvent;
        TouchController.OnTouchEvent += Shot;
        MouseController.OnMouseClickedEvent += Shot;
    }

    void OnDestroy()
    {
        UILevel.LevelChangedEvent -= OnLevelChangedEvent;
        UILevel.OnProgressStepEvent -= OnProgressStepEvent;
        UILevel.EndOfGameEvent -= OnEndOfGameEvent;
        TouchController.OnTouchEvent -= Shot;
        MouseController.OnMouseClickedEvent -= Shot;
    }

    void OnProgressStepEvent(int level)
    {
        ballsCount = StartBallsCount * level;
    }

    void OnLevelChangedEvent(int level, int nextLevel)
    {
        ballsCount = StartBallsCount * nextLevel;
    }

    void OnEndOfGameEvent()
    {
        ballsCount = 0;
    }

    void Shot(Vector3 position)
    {
        if (ballsCount <= 0)
            return;

        GameObject go = SimplePool.Spawn(BallPrefab, startPosition, Quaternion.identity);
        Ball ball = go.GetComponent<Ball>();
        ball.BallLostEvent += OnBallLostEvent;
        Vector3 ballVelocity = ball.Init(position);
        _transform.LookAt(ballVelocity);
        _transform.Rotate(Vector3.right, 90);

        BallsCountChangedEvent?.Invoke(--ballsCount);
    }

    void OnBallLostEvent()
    {
        BallLostEvent?.Invoke(ballsCount);
    }
}
