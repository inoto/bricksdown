﻿using Facebook.Unity;
using UnityEngine;

public class FBController : MonoBehaviour
{
    void Awake()
    {
        if (!FB.IsInitialized)
        {
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            FB.ActivateApp();
        }
    }

    void Start()
    {
        UILevel.LevelChangedEvent += OnLevelChangedEvent;
    }

    void InitCallback()
    {
        if (FB.IsInitialized)
        {
            FB.ActivateApp();
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }

    void OnDestroy()
    {
        UILevel.LevelChangedEvent -= OnLevelChangedEvent;
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }

    void OnLevelChangedEvent(int level, int nextLevel)
    {
        if (FB.IsInitialized && level > 0)
        {
            FB.LogAppEvent(AppEventName.AchievedLevel, level);
            Debug.Log("FB log sent");
        }
    }
}