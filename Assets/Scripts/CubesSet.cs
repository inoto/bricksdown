﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubesSet : MonoBehaviour
{
    public static event Action NoCubesEvent;

    Transform _transform;

    int cubesCount;

    void Awake()
    {
        _transform = GetComponent<Transform>();
    }

    public void Init(int level)
    {
        cubesCount = _transform.childCount;
        for (int i = 0; i < cubesCount; i++)
        {
            Transform child = _transform.GetChild(i);
            child.GetComponent<Cube>().DestroyedEvent += OnCubeDestroyed;
            child.GetComponent<Rigidbody>().mass = level - 0.5f;
        }
    }

    void OnCubeDestroyed(Cube cube)
    {
        cube.DestroyedEvent -= OnCubeDestroyed;
        cubesCount -= 1;
        if (cubesCount == 0)
        {
            Destroy(gameObject);
            NoCubesEvent?.Invoke();
        }
    }
}
