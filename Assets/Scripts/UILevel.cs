﻿using System;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UILevel : MonoBehaviour
{
    public static event Action<int, int> LevelChangedEvent;
    public static event Action<int> OnProgressStepEvent;
    public static event Action EndOfGameEvent;

    [SerializeField] GameObject CubesSetPrefab;
    [SerializeField] GameObject Entities;
    [SerializeField] Image[] ProgressImages;
    [SerializeField] TextMeshProUGUI CurrentLevelCellText;
    [SerializeField] TextMeshProUGUI NextLevelCellText;
    [SerializeField] Color CurrentProgressCellColor;
    [SerializeField] Color CompletedProgressCellColor;
    [SerializeField] Color NotStartedCellColor;
    [SerializeField] GameObject GameOver;
    [SerializeField] GameObject TheEnd;

    Vector3 cubesSetPosition;
    int currentLevel = 0;
    int currentProgress = 0;
    int maxProgress;
    int maxLevel = 10;

    void Start()
    {
        CubesSet.NoCubesEvent += OnNoCubesEvent;
        Cannon.BallLostEvent += OnBallLostEvent;
        cubesSetPosition = CubesSetPrefab.transform.position;

        maxProgress = ProgressImages.Length;

        UpdateProgressCells();
        Invoke("NewLevel", 0.1f);
        FindObjectOfType<CubesSet>().Init(1);
    }

    void OnBallLostEvent(int ballsCount)
    {
        StartCoroutine(CheckNoMoreBalls(ballsCount));
    }

    IEnumerator CheckNoMoreBalls(int ballsCount)
    {
        yield return new WaitForSeconds(4f);

        if (ballsCount <= 0)
        {
            GameOver.SetActive(true);
            yield return new WaitForSeconds(1f);
            RestartGame();
        }

        yield return null;
    }

    void OnDestroy()
    {
        StopAllCoroutines();
        CubesSet.NoCubesEvent -= OnNoCubesEvent;
        Cannon.BallLostEvent -= OnBallLostEvent;
    }

    void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    void OnNoCubesEvent()
    {
        StopAllCoroutines();
        foreach (var ball in FindObjectsOfType<Ball>())
        {
            Destroy(ball.gameObject);
        }

        GameObject go = Instantiate(CubesSetPrefab, cubesSetPosition, Quaternion.identity);
        go.transform.parent = Entities.transform;
        go.GetComponent<CubesSet>().Init(currentLevel);

        OnProgressStepEvent?.Invoke(currentLevel);
        // Debug.Log($"OnProgressStepEvent {currentProgress}");

        currentProgress += 1;
        UpdateProgressCells();
        if (currentProgress >= maxProgress)
        {
            currentProgress = 0;
            UpdateProgressCells();
            NewLevel();
        }
    }

    void UpdateProgressCells()
    {
        for (int i = currentProgress; i < maxProgress; i++)
        {
            ProgressImages[i].color = NotStartedCellColor;
        }
        if (currentProgress > 0)
        {
            ProgressImages[currentProgress-1].color = CompletedProgressCellColor;
        }
        if (currentProgress < maxProgress)
            ProgressImages[currentProgress].color = CurrentProgressCellColor;
    }

    void NewLevel()
    {
        LevelChangedEvent?.Invoke(currentLevel, currentLevel+1);
        // Debug.Log($"LevelChangedEvent from {currentLevel} to {currentLevel+1}");

        currentLevel += 1;
        UpdateLevelCells();

        if (currentLevel >= maxLevel)
        {
            TheEnd.SetActive(true);
            EndOfGameEvent?.Invoke();
        }
    }

    void UpdateLevelCells()
    {
        CurrentLevelCellText.text = currentLevel.ToString();
        NextLevelCellText.text = (currentLevel + 1).ToString();
    }
}
